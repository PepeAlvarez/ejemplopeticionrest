angular.module("starter.services", [])
.factory('UserService', function ($resource) {
    return $resource('http://antonioramirez.esy.es/rest/acontecimientos/:id',{id: "@id"});
});